# -*- coding: utf-8 -*-
import scrapy
import mysql.connector
from scrapy.selector import Selector
from DemoMHC.items import DemomhcItem
from scrapy.loader import ItemLoader
from mysql.connector import errorcode
from datetime import datetime


class QuotesSpider(scrapy.Spider):
    name = "demo"
    allowed_domains = ["ictnews.vietnamnet.vn"]

    def __init__(self):
        print('Init Spider ...')
        self.urls = []
        # Config DB
        conf = {
            'host': 'localhost',
            'user': 'root',
            'password': '',
            'database': 'mhc_demo',
            'port': '3306'
        }

        # Connect
        conn = mysql.connector.connect(**conf)
        cursor = conn.cursor()

        # Fetch Category
        cursor.execute('SELECT slug FROM category')
        rows = cursor.fetchall()
        for row in rows:
            self.urls.append(row[0])

        # Close
        cursor.close()

    def start_requests(self):
        for url in self.urls:
            yield scrapy.Request(url=url)

    def parse(self, response):
        for page in response.xpath('//div[@class="news-list"]/ul/li'):
            page_url = page.xpath(
                'div[@class="grid"]/div[@class="g-content"]/div[@class="g-row"][2]/a/@href').extract_first()
            category = page.xpath(
                'div[@class="grid"]/div[@class="g-content"]/div[@class="g-row"][1]/a/text()').extract_first()
            yield scrapy.Request(url=page_url, callback=self.parsePost, meta={'category_name': category})

    def parsePost(self, response):
        category = response.meta['category_name']
        time = response.xpath(
            '//ol[@class="breadcrumb"]/li[@class="time"]/text()').extract_first()
        time_format = datetime.strptime(
            time, '%H:%M, %d/%m/%Y')

        item = DemomhcItem()
        item['title'] = response.xpath(
            '//div[@class="box-news"]/div[@class="news-title"]/h1/text()').extract_first()
        item['des'] = response.xpath(
            '//div[@class="box-news"]/div[@class="news-desc"]/text()').extract_first().strip()
        item['category'] = category
        item['url'] = response.xpath(
            '//div[@class="bground"]/article/@href').extract_first()
        item['time'] = time_format.strftime("%d/%m/%Y %H:%M:%S")
        item['content'] = response.xpath(
            '//div[@class="box-news"]/div[@class="maincontent"]/div[@class="content-detail"]/div').extract_first().strip()
        item['author'] = response.xpath(
            '//div[@class="box-news"]/div[@class="maincontent"]/p[@align="right"]/b/text()').extract_first()
        yield item
